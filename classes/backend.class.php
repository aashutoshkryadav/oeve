<?php
Class OeVe{
	/**
	 * @var or property OeVe The single instance of the class
	 * 
	 */
	protected static $_instance = null;

	/**
	 * The Constructor.
	 */
	private function __construct() {}
	
	/**
	 * Main OeVe Instance
	 * Ensures only one instance of OeVe is loaded or can be loaded.
	 * @return OeVe - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
 
	/**
	 * Setup : Hooks functions to actions and filters.
	 * Function to set up the backend
	 */
	public function setup() { 
		//register_activation_hook(__FILE__,'cmsse_featured_content_slider_install'); 
		//register_deactivation_hook( __FILE__, 'cmsse_featured_content_slider_uninstall');
		add_action ( 'admin_menu', array(&$this, 'oeve_menu' ));
	}
	
	public function oeve_menu() {
		add_menu_page( 
		__('OeVe Settings'), 
		__('OeVe'), 
		'manage_options', 
		'oeve_settings', 
		array(&$this, 'my_custom_menu_page'), 'dashicons-editor-contract', 81); 
	
	  	add_submenu_page(
		 'oeve_settings',
		  __('OeVe Settings'),
		  __('Settings'),
	 	  'manage_options',
	 	  'oeve_settings',
	 	  array(&$this, 'my_custom_menu_page'));
	   
	   add_submenu_page(
	 	'oeve_settings',
	 	 __('OeVe Help'),
	  	__('Help'),
	   'manage_options',
	   'oeve_help',
	   array(&$this, 'my_custom_menu_page_help'));
	}
	
	public function my_custom_menu_page(){
		echo '<div class="wrap">';
		echo '<h1>Settings</h1>';
		echo '</div>';	
		}
	public function my_custom_menu_page_help(){
		echo '<div class="wrap">';
		echo '<h1>Help</h1>';
		echo '</div>';
		}
	
	
	
}//end of Class