<?php
/*******
Plugin Name: Overlay Exit at Video End
Plugin URI: http://wordpress.org/extend/plugins/oeve/
Description: Overlay Exit at Video End is a plugin for Wordpress to show youtube video in colorbox overlay at start of the website. Overlay automatically closes or exits at end of the YouTube video.
Version: 1.0
Author: Aashtuosh Kumar Yadav
Author URI: http://www.infotokri.in/p/about.html
License: GPL
*/
if(!defined('ABSPATH'))
{
echo "Hi, Sorry you can not play this plugin directly.";
exit;
}

//Path and URL
if ( !defined( 'OEVE_PLUGIN_DIR' ) )
define( 'OEVE_PLUGIN_DIR', WP_PLUGIN_DIR . '/oeve' );
	

// Files inclusion
require( OEVE_PLUGIN_DIR . '/classes/backend.class.php' );

OeVe::instance()->setup();

